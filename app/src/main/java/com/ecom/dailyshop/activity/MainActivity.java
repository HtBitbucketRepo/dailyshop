package com.ecom.dailyshop.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ecom.dailyshop.Fragment.AddressFragment;
import com.ecom.dailyshop.Fragment.MainFragment;
import com.ecom.dailyshop.Fragment.EventListFragment;
import com.ecom.dailyshop.Fragment.SearchListFragment;
import com.ecom.dailyshop.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private HorizontalCalendar horizontalCalendar;
    public static int navItemIndex = 0;
    private static final String TAG_HOME = "home";
    private Handler mHandler;
    private DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    private BottomNavigationView navigation;
    private int tag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigation = findViewById(R.id.btmnav_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        init();
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -2);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 2);

        final Calendar defaultSelectedDate = Calendar.getInstance();

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.LTGRAY, Color.WHITE)
                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffd54f"))
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                .addEvents(new CalendarEventsPredicate() {

                    Random rnd = new Random();

                    @Override
                    public List<CalendarEvent> events(Calendar date) {
                        List<CalendarEvent> events = new ArrayList<>();
                        int count = rnd.nextInt(6);

                        for (int i = 0; i <= count; i++) {
                            events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                        }

                        return events;
                    }
                })
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                String selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                Toast.makeText(MainActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);
            }

        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initCollapsingToolbar();
        loadHomeFragment();
    }


    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitleEnabled(false);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        foo();
    }

    public void foo() {
        if (getSupportFragmentManager().findFragmentById(R.id.sample) instanceof MainFragment) {
            // keep the drawer carat
        } else {
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        toggle.syncState();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            getSupportFragmentManager().popBackStack();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Toast.makeText(this, "back arrow clicked in mainActiity!!", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadHomeFragment() {
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {

                Fragment fragment = getHomeFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                ft.replace(R.id.sample, fragment, null);
                ft.commit();


            }
        };
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                MainFragment fragment = new MainFragment();
                Bundle bundle = new Bundle();
                fragment.setArguments(bundle);
                return fragment;
            default:
                return new MainFragment();
        }
    }


    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_mail:
                    if (getSupportFragmentManager().findFragmentById(R.id.sample) instanceof MainFragment) {
                        return false;
                    }
                    loadFragment(new MainFragment());
                    return true;
                case R.id.navigation_search:
                    if (getSupportFragmentManager().findFragmentById(R.id.sample) instanceof SearchListFragment) {
                        return false;
                    }
                    loadFragment(new SearchListFragment());
                    return true;
                case R.id.navigation_event:
                    if (getSupportFragmentManager().findFragmentById(R.id.sample) instanceof EventListFragment) {
                        return false;
                    }
                    loadFragment(new EventListFragment());
                    return true;
            }

            return false;
        }
    };


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.sample, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if (id == R.id.nav_address) {

            if (getSupportFragmentManager().findFragmentById(R.id.sample) instanceof AddressFragment) {
            } else {
                try {

                    tag = 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (id == R.id.nav_profile) {
            try {

                tag = 0;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.nav_about) {
            try {

                tag = 7;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.nav_addproduct) {
            try {

                tag = 2;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.nav_calender) {
            try {

                tag = 4;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.nav_logout) {
            tag = 9;
            new AlertDialog.Builder(MainActivity.this)
                    .setCancelable(false)
                    .setTitle("Alert!")
                    .setMessage("Do you want to logout?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })

                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        } else if (id == R.id.nav_subscription) {
            try {

                tag = 5;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.nav_support) {
            try {

                tag = 8;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.nav_vacation) {
            try {

                tag = 3;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (tag != 9)
            startActivity(new Intent(this, NavigationItemActivity.class).putExtra("tag", tag));
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
