package com.ecom.dailyshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ecom.dailyshop.Fragment.AboutFragment;
import com.ecom.dailyshop.Fragment.AddressFragment;
import com.ecom.dailyshop.Fragment.CalenderFragment;
import com.ecom.dailyshop.Fragment.CategoryAllListFragment;
import com.ecom.dailyshop.Fragment.CompanyListFragment;
import com.ecom.dailyshop.Fragment.MainFragment;
import com.ecom.dailyshop.Fragment.ProductListFragment;
import com.ecom.dailyshop.Fragment.ProfileFragment;
import com.ecom.dailyshop.Fragment.SubscriptionFragment;
import com.ecom.dailyshop.Fragment.SupportFragment;
import com.ecom.dailyshop.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;


public class NavigationItemActivity extends AppCompatActivity {
    private static final String TAG_HOME = "home";
    private Handler mHandler;
    ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_item);
        init();
        Intent intent = getIntent();
        id = intent.getIntExtra("tag", 10);
        initCollapsingToolbar();
        MainFragment.fromMainScreenclick=false;
        if (id == 1) {
            if (getSupportFragmentManager().findFragmentById(R.id.sample) instanceof AddressFragment) {
            } else {
                try {
                    loadFragment(new CompanyListFragment());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (id == 0) {
            try {
                loadFragment(new ProfileFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == 7) {
            try {
                loadFragment(new AboutFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == 2) {
            try {
                loadFragment(new CategoryAllListFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == 4) {
            try {
                loadFragment(new CalenderFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == 5) {
            try {
                loadFragment(new SubscriptionFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == 8) {
            try {
                loadFragment(new SupportFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == 3) {
            try {
                loadFragment(new CategoryAllListFragment());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitleEnabled(false);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {


        Fragment f = getSupportFragmentManager().findFragmentById(R.id.navigation_frame);
        if (f instanceof ProductListFragment) {
            getSupportFragmentManager().popBackStack();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();
    }


    private void loadHomeFragment() {
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {

                Fragment fragment = getHomeFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.setCustomAnimations(android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right);
                ft.replace(R.id.navigation_frame, fragment, null);
                ft.commit();


            }
        };
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (0) {
            case 0:
                MainFragment fragment = new MainFragment();
                Bundle bundle = new Bundle();
                fragment.setArguments(bundle);
                return fragment;
            default:
                return new MainFragment();
        }
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left,
                            android.R.anim.slide_out_right)
                    .replace(R.id.navigation_frame, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
