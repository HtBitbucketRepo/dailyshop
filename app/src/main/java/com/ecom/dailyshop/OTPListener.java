package com.ecom.dailyshop;

public interface OTPListener {
    void onOTPReceived(String otp);
}