package com.ecom.dailyshop;

import com.ecom.dailyshop.bean.CompanyCategory;
import com.ecom.dailyshop.bean.OtpVerificationInfo;
import com.ecom.dailyshop.bean.UserInfo;
import com.ecom.dailyshop.bean.companylistModal.Company;
import com.ecom.dailyshop.bean.productbycatList.ProductByCategoryList;
import com.ecom.dailyshop.bean.productbycatList.ProductbyCatList;
import com.ecom.dailyshop.bean.productbycmpList.ProductbyCmpList;
import com.ecom.dailyshop.bean.productlistModal.Product;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface {
    @Headers({
            "Accept: */*",
            "Cache-Control:no-cache",
            "Connection:keep-alive",
            "cache-control:no-cache",
    })


    @POST
    Call<UserInfo> getUserInfo(@Body JsonObject info, @Url String url);

    @POST
    Call<OtpVerificationInfo> getOtpValidationStatus(@Body JsonObject otpVerificationInfo, @Url String url);

    @POST
    Call<OtpVerificationInfo> UpdateUserAddress(@Body JsonObject otpVerificationInfo, @Url String url);

    @GET
    Call<Company> getComapnyList(@Url String url);

    @GET
    Call<CompanyCategory> getCategoryList(@Url String url);
    @GET
    Call<ProductbyCatList> getProductListByCat(@Url String url);

    @GET
    Call<ProductbyCmpList> getProductListByCmp(@Url String url);
}