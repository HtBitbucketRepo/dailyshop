package com.ecom.dailyshop.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.ecom.dailyshop.APIClient;
import com.ecom.dailyshop.APIInterface;
import com.ecom.dailyshop.R;
import com.ecom.dailyshop.bean.UserInfo;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class LoginFragment extends Fragment implements View.OnClickListener {
    public static final String url = "/lotusamaze_daily/login_mobile_validation.php";
    private ProgressBar loading_spinner;
    private EditText etMobileNo;
    private LinearLayout ll_loading_spinner;
    private LinearLayout ll_msgsend;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_login,
                container, false);
        setActionBarProperty();
        init(view);
        return view;
    }

    private void setActionBarProperty() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Enter mobile Number");
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    private void init(View view) {
        ll_loading_spinner = (LinearLayout) view.findViewById(R.id.ll_loading_spinner);
        ll_msgsend = (LinearLayout) view.findViewById(R.id.ll_msgsend);
        loading_spinner = (ProgressBar) view.findViewById(R.id.loading_spinner);
        etMobileNo = (EditText) view.findViewById(R.id.etMobileNo);
        Button btnLogin = (Button) view.findViewById(R.id.btnLogin);
        // TextView tvSignup = (TextView) view.findViewById(R.id.tvSignup);
        btnLogin.setOnClickListener(this);
        // String text = "<font color=#cc0029>If you are not registered.click here to </font> <font color=#ffcc00>Signup</font>";
        // tvSignup.setText(Html.fromHtml(text));
    }

    @Override
    public void onClick(View v) {
        ll_loading_spinner.setVisibility(View.VISIBLE);
        ll_msgsend.setVisibility(View.GONE);
        if (v.getId() == R.id.btnLogin) {
            hideKeyboard();
            if (getActivity().getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
            if (etMobileNo.getText().toString().trim().equalsIgnoreCase("") || etMobileNo.getText().length() < 10) {
                Toast.makeText(getActivity(), "Please enter a valid mobile number.", Toast.LENGTH_SHORT).show();
                ll_loading_spinner.setVisibility(View.GONE);
                ll_msgsend.setVisibility(View.VISIBLE);
            } else {
                MobileValidationForSignupApiCall(v, APIClient.getClient("https://www.lotusamaze.com").create(APIInterface.class), url);

            }


        }
    }

    private void hideKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void MobileValidationForSignupApiCall(final View v, APIInterface apiInterface, String url) {
        JsonObject obj = new JsonObject();
        obj.addProperty("mobile_no", etMobileNo.getText().toString());
        Call<UserInfo> call2 = apiInterface.getUserInfo(obj, url);
        call2.enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                ll_loading_spinner.setVisibility(View.GONE);
                ll_msgsend.setVisibility(View.VISIBLE);
                if (response.code() == 200) {
                    try {
                        OTPFragment fragment = new OTPFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("mobile_no", etMobileNo.getText().toString());
                        fragment.setArguments(bundle);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.sample, fragment, "")
                                .addToBackStack(null).commit();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (response.body().getStatusID().equalsIgnoreCase("SU")) {
                        Toast.makeText(getActivity(), "Api StatusID : " + response.body().getStatusID(), Toast.LENGTH_SHORT).show();

                        UserInfo data = response.body();
                        try {
                            OTPFragment fragment = new OTPFragment();
                            Bundle bundle = new Bundle();
                            fragment.setArguments(bundle);
                            getFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.sample, fragment, "")
                                    .addToBackStack(null).commit();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //  startActivity(new Intent(getActivity(), PinActivity.class));
                    } else {
                        Toast.makeText(getActivity(), "Api Response : " + response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                ll_loading_spinner.setVisibility(View.GONE);
                ll_msgsend.setVisibility(View.VISIBLE);
                call.cancel();

            }
        });
    }
}


