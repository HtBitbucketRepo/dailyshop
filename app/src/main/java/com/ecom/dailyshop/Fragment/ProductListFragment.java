package com.ecom.dailyshop.Fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecom.dailyshop.APIClient;
import com.ecom.dailyshop.APIInterface;
import com.ecom.dailyshop.GridSpacingItemDecoration;
import com.ecom.dailyshop.adapter.ProductListAdapter;
import com.ecom.dailyshop.adapter.ProductListByCmpAdapter;
import com.ecom.dailyshop.R;
import com.ecom.dailyshop.RecyclerTouchListener;
import com.ecom.dailyshop.bean.productbycatList.ProductByCategoryList;
import com.ecom.dailyshop.bean.productbycatList.ProductbyCatList;
import com.ecom.dailyshop.bean.productbycmpList.ProductByCompanyList;
import com.ecom.dailyshop.bean.productbycmpList.ProductbyCmpList;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductListFragment extends Fragment {
    private static final String URL_FOR_PRODUCT_LIST_BY_CATEGORY_ID = "/lotusamaze_daily/product_list_by_category.php?category_id=";
    private static final String URL_FOR_PRODUCT_LIST_BY_COMPANY_ID = "/lotusamaze_daily/product_list_by_company.php?company_id=";

    private ArrayList<ProductByCategoryList> coursedetailsList = new ArrayList<>();
    private ArrayList<ProductByCompanyList> coursedetailsList0 = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProductListAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private ProductbyCmpList data0;
    private ProductbyCatList data;
    private ProductListByCmpAdapter mAdapter0;
    private int category_Id;
    private int comp_Id;
    private int company;
    private GridLayoutManager layoutManager;


    public ProductListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_all_list_fragment,
                container, false);
        setActionBarProperty();
        Bundle b = getArguments();
        category_Id = b.getInt("category_Id");
        comp_Id = b.getInt("company_Id");
        company = b.getInt("company");

        init(view);
        if (company == 1)
            getProductListByCategoryId(APIClient.getClient("https://www.lotusamaze.com").create(APIInterface.class), view);
        if (company == 0)
            getProductListByCompanyId(APIClient.getClient("https://www.lotusamaze.com").create(APIInterface.class), view);

        return view;
    }

    private void setActionBarProperty() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Product List");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
         layoutManager = new GridLayoutManager(recyclerView.getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());




        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(view.getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    private void getProductListByCategoryId(APIInterface apiInterface, final View view) {
        String url = URL_FOR_PRODUCT_LIST_BY_CATEGORY_ID + category_Id;
        Call<ProductbyCatList> call2 = apiInterface.getProductListByCat(url);
        call2.enqueue(new Callback<ProductbyCatList>() {
            @Override
            public void onResponse(Call<ProductbyCatList> call, Response<ProductbyCatList> response) {
                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
                if (response.code() == 200) {
                    data = response.body();
                    if (data.getStatusID().equalsIgnoreCase("SU")) {
                        coursedetailsList = data.getProductByCategoryList();
                        mAdapter = new ProductListAdapter(coursedetailsList, getActivity());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getActivity(), data.getStatusMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(view, jObjError.getString("error_description"));
                    } catch (Exception e) {
                        showSnackBar(view, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductbyCatList> call, Throwable t) {
                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
                if (!t.getMessage().equalsIgnoreCase(""))
                    showSnackBar(view, t.getMessage());
                call.cancel();
            }
        });
    }

    private void getProductListByCompanyId(APIInterface apiInterface, final View view) {
        String url = URL_FOR_PRODUCT_LIST_BY_COMPANY_ID + comp_Id;
        Call<ProductbyCmpList> call2 = apiInterface.getProductListByCmp(url);
        call2.enqueue(new Callback<ProductbyCmpList>() {
            @Override
            public void onResponse(Call<ProductbyCmpList> call, Response<ProductbyCmpList> response) {
                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
                if (response.code() == 200) {
                    data0 = response.body();
                    if (data0.getStatusID().equalsIgnoreCase("SU")) {
                        coursedetailsList0 = data0.getProductByCompanyList();
                        mAdapter0 = new ProductListByCmpAdapter(coursedetailsList0, getActivity());
                        recyclerView.setAdapter(mAdapter0);
                        mAdapter0.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getActivity(), data0.getStatusMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(view, jObjError.getString("error_description"));
                    } catch (Exception e) {
                        showSnackBar(view, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductbyCmpList> call, Throwable t) {
                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
                if (!t.getMessage().equalsIgnoreCase(""))
                    showSnackBar(view, t.getMessage());
                call.cancel();
            }
        });
    }

    private void showSnackBar(View view, String msg) {
        if (getActivity() != null) {
            view.findViewById(R.id.ll_no_data).setVisibility(View.VISIBLE);

        }
    }

}
