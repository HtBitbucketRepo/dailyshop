package com.ecom.dailyshop.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.ecom.dailyshop.R;

public class EventListFragment extends Fragment {
//    private static final String URL1 = "v1.0/me/events";
//    private ArrayList<Value> coursedetailsList = new ArrayList<>();
//    private RecyclerView recyclerView;
//    private EventListAdapter mAdapter;
//    private LinearLayoutManager mLayoutManager;
//    GraphApiResponse obj1;
//    Date date;

    public EventListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_list_fragment,
                container, false);
       // setActionBarProperty();
       // init(view);
        //if (CommonUtil.fetchAccessToken(getActivity())==null || CommonUtil.fetchAccessToken(getActivity()).equalsIgnoreCase("null")) {
        //    CommonUtil.showDialog(view, getActivity(), "Session Expire ,please login again.");
       // } else {
       //     prepareCourseData(view);
       // }
        Log.e("Frgament:: ","eventlist fragment");
        return view;
    }}
//
//    private void setActionBarProperty() {
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Event");
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
//    }
//
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//    }
//
//    private void init(View view) {
//        recyclerView = view.findViewById(R.id.recycler_view);
//        recyclerView.setHasFixedSize(true);
//        mLayoutManager = new LinearLayoutManager(view.getContext());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(), LinearLayoutManager.VERTICAL));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//
//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(view.getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                EventDetailFragment fragment = new EventDetailFragment();
//                Bundle bundle = new Bundle();
//                try {
//                    bundle.putString("txtEnd", dateConverter(obj1.getValue().get(position).getEnd().getDateTime(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "EEEE, dd MMMM ")
//                    );
//                    bundle.putString("txtStart", dateConverter(obj1.getValue().get(position).getStart().getDateTime(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "EEEE, dd MMMM ")
//                    );
//                    bundle.putString("txtDisplayName", obj1.getValue().get(position).getLocation().getDisplayName());
//                    bundle.putString("txtSubject", obj1.getValue().get(position).getSubject());
//                    bundle.putString("txtAttendee", obj1.getValue().get(position).getAttendees().get(position).getEmailAddress().getName());
//                    fragment.setArguments(bundle);
//                    getFragmentManager()
//                            .beginTransaction()
//                            .replace(R.id.sample, fragment, "")
//                            .addToBackStack(null).commit();
//                } catch (Exception e) {
//                }
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));
//    }
//
//    private void prepareCourseData(final View view) {
//        APIInterface apiInterface;
//        apiInterface = APIClient.getClient("https://graph.microsoft.com/").create(APIInterface.class);
//        prepareEventListUsingApi(view, apiInterface);
//    }
//
//    private void prepareEventListUsingApi(final View view, APIInterface apiInterface) {
//        String url = URL1;
//        Call<GraphApiResponse> call2 = apiInterface.getEventList(url, CommonUtil.fetchAccessToken(getActivity()));
//        call2.enqueue(new Callback<GraphApiResponse>() {
//            @Override
//            public void onResponse(Call<GraphApiResponse> call, Response<GraphApiResponse> response) {
//                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
//                if (response.code() == 200) {
//                    obj1 = response.body();
//                    coursedetailsList = obj1.getValue();
//                    mAdapter = new EventListAdapter(coursedetailsList);
//                    recyclerView.setAdapter(mAdapter);
//                    mAdapter.notifyDataSetChanged();
//                } else {
//                    try {
//                        JSONObject jObjError = new JSONObject(response.errorBody().string());
//                        showSnackBar(view, jObjError.getString("error_description"));
//                    } catch (Exception e) {
//                        showSnackBar(view, e.getMessage());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GraphApiResponse> call, Throwable t) {
//                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
//                if (!t.getMessage().equalsIgnoreCase(""))
//                    showSnackBar(view, t.getMessage());
//                call.cancel();
//            }
//        });
//
//    }
//
//    private void showSnackBar(View view, String msg) {
//        if (getActivity() != null) {
//            CommonUtil.showDialog(view, getActivity(), msg);
//            view.findViewById(R.id.ll_no_data).setVisibility(View.VISIBLE);
//
//            CommonUtil.saveAccessToken(null,getActivity());
//            Intent intent = new Intent(getActivity(), LoginActivity.class);
//            getActivity().startActivity(intent);
//            getActivity().finish();
//        }
//    }
//
//    private String dateConverter(String dateToConvert, String FromPattern, String ToPattern) {
//        DateFormat originalFormat = new SimpleDateFormat(FromPattern, Locale.US);
//        DateFormat targetFormat = new SimpleDateFormat(ToPattern);
//        try {
//            date = originalFormat.parse(dateToConvert);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return targetFormat.format(date);
//    }
//
//}
