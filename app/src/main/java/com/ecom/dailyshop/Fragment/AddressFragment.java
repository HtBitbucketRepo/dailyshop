package com.ecom.dailyshop.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.ecom.dailyshop.APIClient;
import com.ecom.dailyshop.APIInterface;
import com.ecom.dailyshop.R;
import com.ecom.dailyshop.activity.MainActivity;
import com.ecom.dailyshop.bean.OtpVerificationInfo;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressFragment extends Fragment implements View.OnClickListener {
    public static final String url = "/lotusamaze_daily/user_update_details.php";
    private RelativeLayout rlDetails;
    private ProgressBar loading_spinner;

    public AddressFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.address_fragment,
                container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Address");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        Log.e("Frgament:: ", "address fragment");
        init(view);
        return view;
    }

    private void init(View view) {
        rlDetails = (RelativeLayout) view.findViewById(R.id.rlDetails);
        loading_spinner = (ProgressBar) view.findViewById(R.id.loading_spinner);


        Button btnConfirm = view.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);
        Button btnSkip = view.findViewById(R.id.btnSkip);
        btnSkip.setOnClickListener(this);

    }

    private void ValidateOTPApi(final View v, APIInterface apiInterface, String url) {

        JsonObject obj = new JsonObject();
        obj.addProperty("user_id", "");
        obj.addProperty("full_name", "");
        obj.addProperty("address", "");
        obj.addProperty("city", "");
        obj.addProperty("pin_code", "");

        Call<OtpVerificationInfo> call2 = apiInterface.getOtpValidationStatus(obj, url);
        call2.enqueue(new Callback<OtpVerificationInfo>() {
            @Override
            public void onResponse(Call<OtpVerificationInfo> call, Response<OtpVerificationInfo> response) {
                loading_spinner.setVisibility(View.GONE);
                if (response.code() == 200) {

                    if (response.body().getStatusID().equalsIgnoreCase("SU")) {
                        Toast.makeText(getActivity(), "Api StatusID : " + response.body().getStatusID(), Toast.LENGTH_SHORT).show();
                        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));

                    } else {
                        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));

                        Toast.makeText(getActivity(), "Api Response : " + response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<OtpVerificationInfo> call, Throwable t) {
                loading_spinner.setVisibility(View.GONE);
                call.cancel();

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnConfirm) {
            loading_spinner.setVisibility(View.VISIBLE);
            ValidateOTPApi(view, APIClient.getClient("https://www.lotusamaze.com").create(APIInterface.class), url);
        }

        if(view.getId()==R.id.btnSkip)
        {
            loadFragment(new MainFragment());
        }
    }
    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.sample, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }
}
