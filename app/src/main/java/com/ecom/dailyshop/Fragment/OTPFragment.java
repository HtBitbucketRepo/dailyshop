package com.ecom.dailyshop.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.ecom.dailyshop.APIClient;
import com.ecom.dailyshop.APIInterface;
import com.ecom.dailyshop.IncomingSms;
import com.ecom.dailyshop.activity.MainActivity;
import com.ecom.dailyshop.OTPListener;
import com.ecom.dailyshop.R;
import com.ecom.dailyshop.bean.OtpVerificationInfo;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class OTPFragment extends Fragment implements View.OnClickListener {

    private EditText editText1;
    private EditText[] editTexts;
    public static final String url = "/lotusamaze_daily/login_otp_validation.php";
    private Button btnConfirm;
    private Button btnResend;
    private TextView tvResend;
    private ProgressBar loading_spinner;
    String mobile;
    private LinearLayout ll_otpsend;
    private LinearLayout ll_checkotp;

    public OTPFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pin,
                container, false);
        setActionBarProperty();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mobile = bundle.getString("mobile_no");
        }

        init(view);
        IncomingSms.bindListener(new OTPListener() {
            @Override
            public void onOTPReceived(String extractedOTP) {
                try {
                    editText1.setText(extractedOTP);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        IncomingSms.unbindListener();
        super.onDestroy();
    }

    private void init(View view) {
        ll_otpsend = (LinearLayout) view.findViewById(R.id.ll_otpsend);
        ll_checkotp = (LinearLayout) view.findViewById(R.id.ll_checkotp);
        tvResend = (TextView) view.findViewById(R.id.tvresend);

        btnConfirm = (Button) view.findViewById(R.id.btnConfirm);
        btnResend = (Button) view.findViewById(R.id.btnResend);
        btnConfirm = (Button) view.findViewById(R.id.btnConfirm);
        btnResend = (Button) view.findViewById(R.id.btnResend);
        editText1 = (EditText) view.findViewById(R.id.otpEdit1);
        loading_spinner = (ProgressBar) view.findViewById(R.id.loading_spinner);

        btnConfirm.setOnClickListener(this);
        btnResend.setOnClickListener(this);

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvResend.setText("Waiting " + millisUntilFinished / 1000 + "s " + " before you can resend the code.");
                btnResend.setEnabled(false);
                btnResend.setBackgroundColor(Color.parseColor("#d3d3d3"));
                tvResend.setVisibility(View.VISIBLE);
            }

            public void onFinish() {
                btnResend.setEnabled(true);
                btnResend.setBackgroundColor(Color.parseColor("#d7d7d7"));
                tvResend.setVisibility(View.INVISIBLE);
            }
        }.start();

        editTexts = new EditText[]{editText1};
        editText1.setOnKeyListener(new PinOnKeyListener(0));
    }

    private void setActionBarProperty() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Signing In");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConfirm:
                validateOtpApi(v);
                break;

            case R.id.btnResend:
                validateOtpApi(v);
                break;

            default:
                break;
        }
    }

    private void validateOtpApi(View v) {
        ll_checkotp.setVisibility(View.VISIBLE);
        ll_otpsend.setVisibility(View.GONE);
        String data = editText1.getText().toString();
        Log.e("data::", data);
        ValidateOTPApi(v, APIClient.getClient("https://www.lotusamaze.com").create(APIInterface.class), url);
    }


    public class PinTextWatcher implements TextWatcher {

        private int currentIndex;
        private boolean isFirst = false, isLast = false;
        private String newTypedString = "";

        PinTextWatcher(int currentIndex) {
            this.currentIndex = currentIndex;

            if (currentIndex == 0)
                this.isFirst = true;
            else if (currentIndex == editTexts.length - 1)
                this.isLast = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            newTypedString = s.subSequence(start, start + count).toString().trim();
        }

        @Override
        public void afterTextChanged(Editable s) {

            String text = newTypedString;

            /* Detect paste event and set first char */
            if (text.length() > 1)
                text = String.valueOf(text.charAt(0)); // TODO: We can fill out other EditTexts

            editTexts[currentIndex].removeTextChangedListener(this);
            editTexts[currentIndex].setText(text);
            editTexts[currentIndex].setSelection(text.length());
            editTexts[currentIndex].addTextChangedListener(this);

            if (text.length() == 1)
                moveToNext();
            else if (text.length() == 0)
                moveToPrevious();
        }

        private void moveToNext() {
            if (!isLast)
                editTexts[currentIndex + 1].requestFocus();

            if (isAllEditTextsFilled() && isLast) { // isLast is optional
                editTexts[currentIndex].clearFocus();
                hideKeyboard();
            }
        }

        private void moveToPrevious() {
            if (!isFirst)
                editTexts[currentIndex - 1].requestFocus();
        }

        private boolean isAllEditTextsFilled() {
            for (EditText editText : editTexts)
                if (editText.getText().toString().trim().length() == 0)
                    return false;
            return true;
        }

        private void hideKeyboard() {
            if (getActivity().getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }

    }

    private void ValidateOTPApi(final View v, APIInterface apiInterface, String url) {

        JsonObject obj = new JsonObject();
        obj.addProperty("mobile_no", mobile);
        obj.addProperty("signup_otp", editText1.getText().toString());

        Call<OtpVerificationInfo> call2 = apiInterface.getOtpValidationStatus(obj, url);
        call2.enqueue(new Callback<OtpVerificationInfo>() {
            @Override
            public void onResponse(Call<OtpVerificationInfo> call, Response<OtpVerificationInfo> response) {
                ll_checkotp.setVisibility(View.GONE);
                ll_otpsend.setVisibility(View.VISIBLE);
                if (response.code() == 200) {

                    if (response.body().getStatusID().equalsIgnoreCase("SU")) {
                        Toast.makeText(getActivity(), "Api StatusID : " + response.body().getStatusID(), Toast.LENGTH_SHORT).show();
                        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));

                    } else {
                        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));

                        Toast.makeText(getActivity(), "Api Response : " + response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<OtpVerificationInfo> call, Throwable t) {
                ll_checkotp.setVisibility(View.GONE);
                ll_otpsend.setVisibility(View.VISIBLE);
                call.cancel();

            }
        });
    }

    public class PinOnKeyListener implements View.OnKeyListener {

        private int currentIndex;

        PinOnKeyListener(int currentIndex) {
            this.currentIndex = currentIndex;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_UP) {
                if (editTexts[currentIndex].getText().toString().isEmpty() && currentIndex != 0)
                    editTexts[currentIndex - 1].requestFocus();
            }
            return false;
        }

    }

}


