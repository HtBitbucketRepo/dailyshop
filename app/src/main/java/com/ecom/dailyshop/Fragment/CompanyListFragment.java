package com.ecom.dailyshop.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecom.dailyshop.APIClient;
import com.ecom.dailyshop.APIInterface;
import com.ecom.dailyshop.adapter.CompanyListAdapter;
import com.ecom.dailyshop.R;
import com.ecom.dailyshop.RecyclerTouchListener;
import com.ecom.dailyshop.bean.companylistModal.Company;
import com.ecom.dailyshop.bean.companylistModal.CompanyList;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CompanyListFragment extends Fragment {
    private static final String URL_FOR_COMPANY_LIST = "/lotusamaze_daily/company_all_list.php";

    private ArrayList<CompanyList> coursedetailsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CompanyListAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private Company data;


    public CompanyListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_all_list_fragment,
                container, false);
        setActionBarProperty();

        init(view);
        getCompanyList( APIClient.getClient("https://www.lotusamaze.com").create(APIInterface.class), view);

        return view;
    }

    private void setActionBarProperty() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Company List");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(),0));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(view.getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                String category_Id=coursedetailsList.get(position).getCompanyId();
                try {
                    loadFragment(new ProductListFragment(),category_Id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private boolean loadFragment(Fragment fragment, String category_Id) {
        Bundle args = new Bundle();
        args.putInt("company_Id", Integer.parseInt(category_Id));
        args.putInt("company", 0);
        if (fragment != null) {
            fragment.setArguments(args);
            if(MainFragment.fromMainScreenclick){ getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.sample, fragment)
                    .addToBackStack(null)
                    .commit();}
            else{ getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.navigation_frame, fragment)
                    .addToBackStack(null)
                    .commit();}
            return true;
        }
        return false;
    }
    private void getCompanyList(APIInterface apiInterface, final View view) {
        String url = URL_FOR_COMPANY_LIST;
        Call<Company> call2 = apiInterface.getComapnyList(url);
        call2.enqueue(new Callback<Company>() {
            @Override
            public void onResponse(Call<Company> call, Response<Company> response) {
                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
                if (response.code() == 200) {
                    data = response.body();
                    coursedetailsList=data.getCompanyList();
                    mAdapter = new CompanyListAdapter(coursedetailsList,getActivity());
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showSnackBar(view, jObjError.getString("error_description"));
                    } catch (Exception e) {
                        showSnackBar(view, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Company> call, Throwable t) {
                view.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
                if (!t.getMessage().equalsIgnoreCase(""))
                    showSnackBar(view, t.getMessage());
                call.cancel();
            }
        });
    }

    private void showSnackBar(View view, String msg) {
        if (getActivity() != null) {
            view.findViewById(R.id.ll_no_data).setVisibility(View.VISIBLE);

        }
    }

}
