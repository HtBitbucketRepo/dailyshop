
package com.ecom.dailyshop.bean;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyCategory {

    @SerializedName("StatusID")
    @Expose
    private String statusID;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("ProductCategoryList")
    @Expose
    private ArrayList<ProductCategoryList> productCategoryList = null;

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<ProductCategoryList> getProductCategoryList() {
        return productCategoryList;
    }

    public void setProductCategoryList(ArrayList<ProductCategoryList> productCategoryList) {
        this.productCategoryList = productCategoryList;
    }

}
