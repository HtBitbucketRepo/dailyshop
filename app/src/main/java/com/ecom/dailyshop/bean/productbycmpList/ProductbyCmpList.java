
package com.ecom.dailyshop.bean.productbycmpList;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductbyCmpList {

    @SerializedName("StatusID")
    @Expose
    private String statusID;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("ProductByCompanyList")
    @Expose
    private ArrayList<ProductByCompanyList> productByCompanyList = null;

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<ProductByCompanyList> getProductByCompanyList() {
        return productByCompanyList;
    }

    public void setProductByCompanyList(ArrayList<ProductByCompanyList> productByCompanyList) {
        this.productByCompanyList = productByCompanyList;
    }

}
