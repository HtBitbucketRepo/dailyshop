package com.ecom.dailyshop.bean;

public class OtpVerificationInfo {
    public String getStatusID() {
        return StatusID;
    }

    public void setStatusID(String statusID) {
        StatusID = statusID;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        StatusMessage = statusMessage;
    }

    String StatusID;
    String StatusMessage;
}
