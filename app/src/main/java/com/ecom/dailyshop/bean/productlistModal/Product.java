
package com.ecom.dailyshop.bean.productlistModal;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("StatusID")
    @Expose
    private String statusID;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("ProductAllList")
    @Expose
    private ArrayList<ProductAllList> productAllList = null;

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<ProductAllList> getProductAllList() {
        return productAllList;
    }

    public void setProductAllList(ArrayList<ProductAllList> productAllList) {
        this.productAllList = productAllList;
    }

}
