
package com.ecom.dailyshop.bean.productbycatList;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductbyCatList {

    @SerializedName("StatusID")
    @Expose
    private String statusID;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("CompanyList")
    @Expose
    private ArrayList<CompanyList> companyList = null;
    @SerializedName("ProductByCategoryList")
    @Expose
    private ArrayList<ProductByCategoryList> productByCategoryList = null;

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<CompanyList> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(ArrayList<CompanyList> companyList) {
        this.companyList = companyList;
    }

    public ArrayList<ProductByCategoryList> getProductByCategoryList() {
        return productByCategoryList;
    }

    public void setProductByCategoryList(ArrayList<ProductByCategoryList> productByCategoryList) {
        this.productByCategoryList = productByCategoryList;
    }

}
