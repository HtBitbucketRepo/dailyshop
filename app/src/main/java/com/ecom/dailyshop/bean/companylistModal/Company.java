
package com.ecom.dailyshop.bean.companylistModal;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Company {

    @SerializedName("StatusID")
    @Expose
    private String statusID;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("CompanyList")
    @Expose
    private ArrayList<CompanyList> companyList = null;

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<CompanyList> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(ArrayList<CompanyList> companyList) {
        this.companyList = companyList;
    }

}
