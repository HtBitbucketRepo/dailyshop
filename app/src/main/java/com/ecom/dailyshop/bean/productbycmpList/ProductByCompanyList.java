
package com.ecom.dailyshop.bean.productbycmpList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductByCompanyList {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("product_price")
    @Expose
    private String productPrice;
    @SerializedName("product_weight")
    @Expose
    private String productWeight;
    @SerializedName("product_image_url")
    @Expose
    private String productImageUrl;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(String productWeight) {
        this.productWeight = productWeight;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

}
