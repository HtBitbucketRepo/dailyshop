package com.ecom.dailyshop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.ecom.dailyshop.R;
import com.ecom.dailyshop.bean.productbycatList.ProductByCategoryList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {
    private FragmentActivity activity;
    private ArrayList<ProductByCategoryList> courselist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvPrice;
        public TextView tvWeight;
        public ImageView ivCompanylogo;


        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            ivCompanylogo = view.findViewById(R.id.ivCompanylogo);
            tvPrice=view.findViewById(R.id.tvPrice);
            tvWeight=view.findViewById(R.id.tvWeight);
        }
    }

    public ProductListAdapter(ArrayList<ProductByCategoryList> courselist, FragmentActivity activity) {
        this.courselist = courselist;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_detail_item_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProductByCategoryList companyInfo = courselist.get(position);
        holder.tvName.setText(companyInfo.getProductName());
        holder.tvPrice.setText(companyInfo.getProductPrice());
        holder.tvWeight.setText(companyInfo.getProductWeight());
        Picasso.with(activity).load(companyInfo.getProductImageUrl()).into(holder.ivCompanylogo);

    }

    @Override
    public int getItemCount() {
        return courselist.size();
    }
}
