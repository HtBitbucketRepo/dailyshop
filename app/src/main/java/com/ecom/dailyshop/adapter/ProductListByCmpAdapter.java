package com.ecom.dailyshop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.ecom.dailyshop.R;
import com.ecom.dailyshop.bean.productbycmpList.ProductByCompanyList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductListByCmpAdapter extends RecyclerView.Adapter<ProductListByCmpAdapter.MyViewHolder> {
    private FragmentActivity activity;
    private ArrayList<ProductByCompanyList> courselist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public ImageView ivCompanylogo;


        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            ivCompanylogo = view.findViewById(R.id.ivCompanylogo);
        }
    }

    public ProductListByCmpAdapter(ArrayList<ProductByCompanyList> courselist, FragmentActivity activity) {
        this.courselist = courselist;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_detail_item_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProductByCompanyList companyInfo = courselist.get(position);
        holder.tvName.setText(companyInfo.getProductName());
        Picasso.with(activity).load(companyInfo.getProductImageUrl()).into(holder.ivCompanylogo);

    }

    @Override
    public int getItemCount() {
        return courselist.size();
    }
}
