package com.ecom.dailyshop;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;



public class MyService extends Service {

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MyThread myThread = new MyThread();
        myThread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    public class MyThread extends Thread {
        @Override
        public void run() {
            Intent intent = new Intent();
            intent.setAction("MY_ACTION");
            intent.setClass(getApplicationContext(), MyReceiver.class);
            sendBroadcast(intent);
        }
    }

}
