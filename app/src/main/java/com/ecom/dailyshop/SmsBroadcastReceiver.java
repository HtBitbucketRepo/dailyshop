package com.ecom.dailyshop;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A broadcast receiver who listens for incoming SMS
 */
public class SmsBroadcastReceiver extends BroadcastReceiver {

    private String phoneNumber;
    private String senderNum;
    private String message;

    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        Log.e("inside receiver","ok");
        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    senderNum = phoneNumber;
                    message = currentMessage.getDisplayMessageBody();
                    Toast.makeText(context, "New sms received from : " + senderNum + " Message :" + message, Toast.LENGTH_SHORT).show();
                }

                //Start our own service
                Intent intent2 = new Intent(context,
                        MyService.class);
                context.startService(intent2);
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }

    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:a");
        String strDate = mdformat.format(calendar.getTime());
        Log.e("strDate :", strDate);
        return strDate;
    }

    public String getContactName(final String phoneNumber, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contactName = "";
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }
        return contactName;
    }

    public String printInitials(String name) {
        String firstName;
        if (name.split("\\w+").length > 1) {

            firstName = name.substring(0, name.lastIndexOf(' '));
        } else {
            firstName = name;
        }
        return Character.toString(firstName.charAt(0));
    }
}

